# Stuff that needs doing

##VIM
* airline
  * check for powerline symbols / patched font

## TMUX Status Line
* check for powerline symbols / patched font
* fix uptime so it account properly for being up over 24 hours
* battery that is reliable across os / device
